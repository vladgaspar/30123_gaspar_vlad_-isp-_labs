package isp.lab9.exercise2;

import java.util.Random;

public class HumiditySensor extends Observable {

    private int humidity;

    public void readSensor(){
        Random x=new Random();
        humidity=x.nextInt(100);
        this.changeState(humidity);
    }

    public int getHumidity() {return this.humidity;}

}
