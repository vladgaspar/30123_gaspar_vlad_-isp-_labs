package isp.lab9.exercise2;

import java.util.Random;

public class PreassureSensor extends Observable {

    private double preassure;

    public void readSensor(){
        Random x=new Random();
        preassure=x.nextInt(1000)+500;
        preassure/=1000;
        this.changeState(preassure);
    }

}
