package isp.lab9.exercise2;


public class Controller implements Observer {

    public void update(Object event){
        System.out.println("Sensor status has changed!");
        System.out.println("Received value : " + event.toString() );
    }

}

