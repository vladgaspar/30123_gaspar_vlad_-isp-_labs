package isp.lab9.exercise2;

import java.util.Random;

public class TemperatureSensor extends Observable{

    private int temperature;

    public void readSensor() {
        Random x = new Random();
        temperature = -20 + x.nextInt(50);
        this.changeState(temperature);
    }

}
