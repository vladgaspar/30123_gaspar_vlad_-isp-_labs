package isp.lab3.exercise5;


import java.util.Scanner;

public class Exercise5 {

    public static class VendingMachine {

        public String[] product = new String[100];
        public int credit = 0;
        public int numarProduse = 0;

        public void addProducts(String produs) {
            product[numarProduse] = produs ;
            numarProduse++;
        }

        public void displayProducts() {
            for (int i = 0; i < product.length; i++) {
                if (product[i] == null)
                    break;
                System.out.println(product[i] + "Id: " + i);
            }

        }

        public void insertCoin(int value) {
            if (value >= 1)
                credit = credit + value;
        }

        public String selectProduct(int id) {
            for (int i = 0; i < product.length; i++) {
                if (id == i)
                    return product[i];
            }
            return "The id dosen't exist";
        }

        public void displayCredit() {
            System.out.println("Credit: " + credit);
        }

        public void userMenu() {
            Scanner scanner = new Scanner(System.in);

            while (true) {
                int selection;

                System.out.println("Available Products: ");
                System.out.println("---------------------");
                displayProducts();
                System.out.println();
                System.out.println("Please select an option:");
                System.out.println("0 - To quit");
                System.out.println("1 - Insert coins");
                System.out.println("2 - Select a product");
                System.out.println("3 - Display current credit");

                selection = scanner.nextInt();

                if (selection == 1) {
                    System.out.println("Insert ammount: ");
                    int ammount = scanner.nextInt();
                    this.insertCoin(ammount);

                } else if (selection == 2) {
                    System.out.println("Select product: ");
                    int select = scanner.nextInt();
                    System.out.println(this.selectProduct(select));

                } else if (selection == 3) {
                    this.displayCredit();


                } else if (selection == 0){
                    System.out.println("Good bye!");
                    break;
                }
                else System.out.println("Invalid selection. Try again!");
            }

        }
    }


    public static void main(String[] args) {

        VendingMachine vm = new VendingMachine();

        vm.addProducts("Coca-Cola");
        vm.addProducts("Fanta");
        vm.addProducts("Lion");
        vm.addProducts("Mars");

        vm.userMenu();



    }
}
