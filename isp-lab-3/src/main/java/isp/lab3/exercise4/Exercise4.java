package isp.lab3.exercise4;

public class Exercise4 {

    public static class MyPoint {

        public int x;
        public int y;
        public int z;

        public MyPoint(){
            x = 0;
            y = 0;
            z = 0;
        }

        public MyPoint(int x, int y, int z){
            this.x = x;
            this.y = y ;
            this.z = z;
        }

        public int getX() {
            return x;
        }

        public int getY() {
            return y;
        }

        public int getZ() {
            return z;
        }

        public void setX(int x) {
            this.x = x;
        }

        public void setY(int y) {
            this.y = y;
        }

        public void setZ(int z) {
            this.z = z;
        }

        public void setXYZ(int x, int y, int z){
            this.x = x;
            this.y = y;
            this.z = z;
        }

        public double distance(int x, int y, int z){
            return Math.sqrt(Math.pow(this.x - x, 2) + Math.pow(this.y - y, 2) + Math.pow(this.z - z, 2));
        }

        public double distance(MyPoint another){
            return Math.sqrt(Math.pow(this.x - another.x, 2) + Math.pow(this.y - another.y, 2) + Math.pow(this.z - another.z, 2));
        }
    }

    public static void main(String[] args) {

        MyPoint point1 = new MyPoint(1,6,7);
        MyPoint point2 = new MyPoint(4,5,6);

        System.out.println("The distance between the points is: " + point1.distance(point2));


    }

}
