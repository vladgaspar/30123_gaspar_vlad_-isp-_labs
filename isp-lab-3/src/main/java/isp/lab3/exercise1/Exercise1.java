package isp.lab3.exercise1;

public class Exercise1 {


    public static class Tree{

        public int height = 0;

        public Tree(){
            height = 15;
        }

        public void grow(int meters){
            if(meters >= 1)
                height = height + meters  ;
        }

        public String toString(){
            return "height: " + height;
        }

    }

    public static void main(String[] args) {
        Tree tree1 = new Tree();
        Tree tree2 = new Tree();

        tree1.grow(5);
        tree2.grow(8);
        System.out.println(tree1);
        System.out.println(tree2);

    }
}



