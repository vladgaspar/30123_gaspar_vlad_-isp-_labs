package isp.lab3.exercise2;

public class Exercise2 {

    public static class Rectangle {

        private int length = 2;
        private int width = 1;
        private String color = "red";

        Rectangle(int length, int width){
            this.length = length;
            this.width = width;
        }

        Rectangle(int length, int width, String color){
            this.length = length;
            this.width = width ;
            this.color = color;
        }

        public int getLength() {
            return length;
        }

        public int getWidth() {
            return width;
        }

        public String getColor() {
            return color;
        }

        public int getPerimeter(){
            return 2 * width + 2 * length;
        }

        public int getArea(){
            return width * length;
        }
    }

    public static void main(String[] args) {

        Rectangle rectangle = new Rectangle(3,4);
        System.out.println(rectangle.getLength());
        System.out.println(rectangle.getWidth());
        System.out.println(rectangle.getColor());
        System.out.println(rectangle.getPerimeter());
        System.out.println(rectangle.getArea());

    }

}
