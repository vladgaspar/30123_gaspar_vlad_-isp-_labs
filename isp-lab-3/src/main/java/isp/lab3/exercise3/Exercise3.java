package isp.lab3.exercise3;

public class Exercise3 {

    public static class Vehicle{

        private String model;
        private String type;
        private int speed;
        private char fuelType;
        private static int nrOfVehicle = 0;

        public Vehicle(String model, String type, int speed, char fuelType){
            this.model = model;
            this.type = type;
            this.speed = speed;
            this.fuelType = fuelType ;
            nrOfVehicle++;
        }

        public String getModel() {
            return model;
        }

        public String getType() {
            return type;
        }

        public int getSpeed() {
            return speed;
        }

        public char getFuelType() {
            return fuelType;
        }

        public void setModel(String model) {
            this.model = model;
        }

        public void setType(String type) {
            this.type = type;
        }

        public void setSpeed(int speed) {
            this.speed = speed;
        }

        public void setFuelType(char fuelType) {
            this.fuelType = fuelType;
        }

        public String toString(){
            return model + " (" + type + ") speed " + speed + " fuel type " + fuelType;
        }

        public static int displayNumberOfVehicles(){
            return nrOfVehicle;
        }



    }

    @Override
    public boolean equals(Object o){
        if(this == o)
            return true;
        return false;

    }






    public static void main(String[] args) {

        Vehicle audi = new Vehicle("Audi", "A4", 220, 'B');
        Vehicle audi1 = new Vehicle("Audi", "A4", 220, 'B');
        Vehicle bmw = new Vehicle("BMW", "320d",180,'D');

        System.out.println(audi.getModel() + " " + audi.getType() + " " + audi.getSpeed() + " " + audi.getFuelType());
        System.out.println(bmw.getModel() + " " + bmw.getType() + " " + bmw.getSpeed() + " " + bmw.getFuelType());

        System.out.println(audi.equals(audi1));

        System.out.println("Number of Objects: " + Vehicle.displayNumberOfVehicles());



    }
}
