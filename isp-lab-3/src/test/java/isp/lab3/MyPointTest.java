package isp.lab3;


import isp.lab3.exercise4.Exercise4;
import org.junit.Test;
import static org.junit.Assert.assertEquals;

public class MyPointTest {

    Exercise4.MyPoint point = new Exercise4.MyPoint(5,6,3);

    @Test
    public void distanceTest(){
        assertEquals("The distance should be 5: ",5, point.distance(1,3,3),0.0000001) ;

    }
}
