package isp.lab3;

import isp.lab3.exercise1.Exercise1;
import org.junit.Test;

import static org.junit.Assert.assertEquals;


public class TreeTest {

    Exercise1.Tree tree = new Exercise1.Tree();

    @Test
    public void testGrow() {
        tree.grow(2);
        assertEquals("Should be 17", 17,tree.height);
        tree.grow(7);
        assertEquals("Should be 24", 24,tree.height);
        tree.grow(-1);
        assertEquals("Should be 24", 24,tree.height) ;

    }

    @Test
    public void testToString() {

        assertEquals("height: 15",tree.toString());
    }

}
