package isp.lab3;

import isp.lab3.exercise5.Exercise5;
import org.junit.Test;
import static org.junit.Assert.assertEquals;

public class VendingMachineTest {

    Exercise5.VendingMachine vm = new Exercise5.VendingMachine();

    @Test
    public void testDisplayProducts(){
        vm.addProducts("Pepsi");
        vm.addProducts("Kit-Kat");

        assertEquals("Pepsi ID: 0 \n Kit-Kat ID: 1",System.out.println(vm.displayProducts())) ;
    }

    @Test
    public void testInsertCoin(){}

    @Test
    public void testSelectProduct(){}
}
