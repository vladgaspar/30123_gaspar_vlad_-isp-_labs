package isp.lab3;

import isp.lab3.exercise3.Exercise3;
import org.junit.Test;
import static org.junit.Assert.assertEquals;

public class VehicleTest {

    Exercise3.Vehicle vehicle = new Exercise3.Vehicle("Audi","A4",220,'B');

    @Test
    public void toStringTest(){
        assertEquals("Audi (A4) speed 220 fuel type B", vehicle.toString()) ;
    }

}
