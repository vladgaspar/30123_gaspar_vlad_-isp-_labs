package isp.lab4.exercise1;

import org.junit.Test;
import static org.junit.Assert.assertEquals;

public class TemperatureSensorTest {
    //implement minimal tests
    TemperatureSensor temperatureSensor = new TemperatureSensor(200,"Cuptor");

    @Test
    public void getValueTest(){
        assertEquals("Value must be 200: ",200,temperatureSensor.getValue());
    }

    @Test
    public void getLocation(){
        assertEquals("Cuptor",temperatureSensor.getLocation());
    }

    @Test
    public void toStringTest(){
        assertEquals("TemperatureSensor{value=200, location='Cuptor'}",temperatureSensor.toString());
    }

}
