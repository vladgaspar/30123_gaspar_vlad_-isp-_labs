package isp.lab4.exercise2;

import org.junit.Test;
import static org.junit.Assert.assertEquals;

public class FireAlarmTest {

    FireAlarm fireAlarm = new FireAlarm(false);

    @Test
    public void isActiveTest(){
        assertEquals("Should be false",false,fireAlarm.isActive());
    }

    @Test
    public void setActiveTest(){
        fireAlarm.setActive(true);
        assertEquals("Should be true",true,fireAlarm.isActive());
    }

    @Test
    public void toStringTest(){
        assertEquals("FireAlarm{active=false}",fireAlarm.toString());
    }
}