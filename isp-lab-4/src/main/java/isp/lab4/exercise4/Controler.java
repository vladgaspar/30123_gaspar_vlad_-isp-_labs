package isp.lab4.exercise4;

import isp.lab4.exercise1.TemperatureSensor;
import isp.lab4.exercise2.FireAlarm;

public class Controler {

    private TemperatureSensor[] temperatureSensors;

    private FireAlarm fireSensor;

    Controler(TemperatureSensor[] temperatureSensors, FireAlarm fireSensor)
    {
        this.temperatureSensors = temperatureSensors;
        this.fireSensor = fireSensor;
    }

    public void controlSetup(){

        int i,count = 0;

        for (i = 0; i < temperatureSensors.length; i++){
            TemperatureSensor temperatureSensor = temperatureSensors[i];

            if(temperatureSensor.getValue()>50)
                count++;
        }

        if(count == temperatureSensors.length)
        {
            fireSensor.setActive(true);
            System.out.println("Fire alarm started");
        }
        else
            System.out.println("Fire alarm not started");
    }

}

