package isp.lab4.exercise5;

import isp.lab4.exercise1.TemperatureSensor;
import isp.lab4.exercise2.FireAlarm;

public class Controler {

    private TemperatureSensor[] temperatureSensors = new TemperatureSensor[3];

    private FireAlarm fireSensor;

    Controler()
    {
        this.temperatureSensors[0] = new TemperatureSensor(20,"Hol");
        this.temperatureSensors[1] = new TemperatureSensor(40,"Bucatarie");
        this.temperatureSensors[3] = new TemperatureSensor(51,"Baie");
        this.fireSensor = new FireAlarm(false);
    }

    public void controlSetup(){

        int i,count = 0;

        for (i = 0; i < temperatureSensors.length; i++){
            TemperatureSensor temperatureSensor = temperatureSensors[i];

            if(temperatureSensor.getValue()>50)
                count++;
        }

        if(count != 0)
        {
            fireSensor.setActive(true);
            System.out.println("Fire alarm started");
        }
        else
            System.out.println("Fire alarm not started");
    }

}

