package isp.lab4.exercise2;

public class FireAlarm{

    private boolean active;

    public FireAlarm(boolean active){
        this.active = active;
    }

    boolean isActive(){
        return this.active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    @Override
    public String toString() {
        return "FireAlarm{" +
                "active=" + active +
                '}';
    }
}
