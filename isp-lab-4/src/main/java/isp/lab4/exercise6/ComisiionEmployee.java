package isp.lab4.exercise6;

public class ComisiionEmployee extends Employee {

    private double grossSales;
    private double comissionSales;

    public ComisiionEmployee(String firstName, String lastName, double grossSales, double comissionSales) {
        super(firstName, lastName);
        this.comissionSales = comissionSales;
        this.grossSales = grossSales;
    }

    @Override
    public double getPaymentAmount() {
        return grossSales + comissionSales;
    }
}
