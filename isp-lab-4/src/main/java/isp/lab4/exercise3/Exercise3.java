package isp.lab4.exercise3;

import isp.lab4.exercise1.TemperatureSensor;
import isp.lab4.exercise2.FireAlarm;

public class Exercise3 {

    public static void main(String[] args) {
        TemperatureSensor temperatureSensor = new TemperatureSensor(200,"Parcare");
        FireAlarm fireAlarm = new FireAlarm(false);
        Controler controler = new Controler(temperatureSensor,fireAlarm);
        controler.controlStep();
    }
}
