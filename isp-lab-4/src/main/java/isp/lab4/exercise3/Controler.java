package isp.lab4.exercise3;

import isp.lab4.exercise1.TemperatureSensor;
import isp.lab4.exercise2.FireAlarm;

public class Controler {

    private TemperatureSensor temperatureSensor;
    private FireAlarm fireAlarm;

    public void controlStep(){
        if(temperatureSensor.getValue()>50){
            System.out.println("Fire alarm started");
            fireAlarm.setActive(true);
        } else
            System.out.println("Fire alarm not started");

    }

    public Controler(TemperatureSensor temperatureSensor, FireAlarm fireAlarm){
        this.fireAlarm = fireAlarm;
        this.temperatureSensor = temperatureSensor;
    }
}
