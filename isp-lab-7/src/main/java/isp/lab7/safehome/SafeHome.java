package isp.lab7.safehome;

public class SafeHome {

    public static void main(String[] args) throws Exception {

        DoorLockController doorLockController = new DoorLockController();

        doorLockController.addTenant("12345", "Marius");
        doorLockController.addTenant("9876","Alex");
        doorLockController.addTenant("4224","Stefan");

        doorLockController.removeTenant("Marius");
        doorLockController.removeTenant("Marius");
        doorLockController.enterPin("12345");
        doorLockController.enterPin("12345");


        System.out.println(doorLockController.getAccesLog());
    }
}
