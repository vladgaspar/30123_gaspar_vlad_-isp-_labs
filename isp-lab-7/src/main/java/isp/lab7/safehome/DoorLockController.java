package isp.lab7.safehome;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DoorLockController implements ControllerInterface {

    private HashMap<Tenant, AccessKey> validAccess;
    private int attempts = 0;
    private Door door;
    private ArrayList<AccessLog> accessLog;

    public DoorLockController() {
        this.validAccess = new HashMap<>();
        door = new Door();
        accessLog = new ArrayList<>();
    }

    public boolean existingTenant(Tenant tenant){
        for(Tenant i : validAccess.keySet()){
            if(tenant.equals(i))
                return true;
        }
        return false;
    }

    public boolean existingPin(AccessKey accessKey){
        for(Tenant i : validAccess.keySet()){
            if(validAccess.get(i).equals(accessKey))
                return true;
        }
        return false;
    }

    @Override
    public DoorStatus enterPin(String pin) throws InvalidPinException, TooManyAttemptsException {

        AccessKey accessKey = new AccessKey();
        accessKey.setPin(pin);
        AccessLog log = new AccessLog("Unknow", LocalDateTime.now(),"Tried to close/open the door",door.getStatus());

        if(pin.equals(ControllerInterface.MASTER_KEY)){
            attempts = 0;;
            if(door.getStatus() == DoorStatus.CLOSE) door.unlockDoor();
            else door.lockDoor();
            accessLog.add(new AccessLog(ControllerInterface.MASTER_TENANT_NAME,LocalDateTime.now(),"Has closed/opened the door",door.getStatus()));

        } else {

            if(attempts < 2 && existingPin(accessKey)){
                attempts = 0;
                String name = null;
                log.setErrorMessage("Null");
                for(Tenant i : validAccess.keySet()){
                    if(validAccess.get(i).equals(accessKey))
                        name = i.getName();
                }
                accessLog.add(new AccessLog(name, LocalDateTime.now(),"Opened/closed the door",door.getStatus()));
                if(door.getStatus() == DoorStatus.CLOSE) door.unlockDoor();
                else door.lockDoor();

            } else {
                attempts++;
                if(attempts > 2){
                    log.setErrorMessage("Too many attempts");
                    accessLog.add(log);
                    throw new TooManyAttemptsException("Too many attempts");
                }
                    log.setErrorMessage("Invalid Pin");
                    accessLog.add(log);
                    throw new InvalidPinException("Invalid Pin");
            }
        }
        return door.getStatus();
    }

    @Override
    public void addTenant(String pin, String name) throws Exception, TenantAlreadyExistsException {
        Tenant tenant = new Tenant();
        tenant.setName(name);
        AccessKey accessKey = new AccessKey();
        accessKey.setPin(pin);

        AccessLog log = new AccessLog(name,LocalDateTime.now(),"Add Tenant",door.getStatus());

        if(existingTenant(tenant)){
            log.setErrorMessage("Tenant Already Exist");
            accessLog.add(log);
            throw new TenantAlreadyExistsException("Tenant Already Exist");

        } else {
            log.setErrorMessage("Null");
            validAccess.put(tenant,accessKey);
            accessLog.add(log);
        }
    }


    @Override
    public void removeTenant(String name) throws TenantNotFoundException {
        Tenant tenant = new Tenant();
        tenant.setName(name);

        AccessLog log = new AccessLog(name,LocalDateTime.now(),"Remove Tenant",door.getStatus());

        if(existingTenant(tenant)){
            AccessKey accessKey = validAccess.remove(tenant);
            log.setErrorMessage("Null");
            accessLog.add(log);
        } else {
            log.setErrorMessage("Tenant Not Fount");
            accessLog.add(log);
            throw new TenantNotFoundException("Tenant Not Found");
        }

    }

    public List<AccessLog> getAccesLog() {
        return accessLog;
    }
}


