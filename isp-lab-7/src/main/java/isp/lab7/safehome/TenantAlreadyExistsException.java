package isp.lab7.safehome;

public class TenantAlreadyExistsException extends Exception{

    TenantAlreadyExistsException(String message){
        super(message);
    }
}
