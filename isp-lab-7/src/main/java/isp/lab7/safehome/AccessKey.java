package isp.lab7.safehome;

public class AccessKey {
    private String pin;

    public String getPin() {
        return pin;
    }

    public void setPin(String pin) {
        this.pin = pin;
    }

    public boolean equals(AccessKey accessKey) {
        return this.pin.equals(accessKey.getPin());
    }
}
