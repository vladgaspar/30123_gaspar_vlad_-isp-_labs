package isp.lab7.safehome;

import java.time.LocalDateTime;

public class AccessLog {
    private String tenateName;
    private LocalDateTime dateTime;
    private String operation;
    private DoorStatus doorStatus;
    private String errorMessage;

    public AccessLog(String tenateName, LocalDateTime dateTime, String operation, DoorStatus doorStatus){
        this.tenateName = tenateName;
        this.dateTime = dateTime;
        this.operation = operation;
        this.doorStatus = doorStatus;

    }


    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    @Override
    public String toString() {
        return "AccessLog{" +
                "tenateName='" + tenateName + '\'' +
                ", dateTime=" + dateTime +
                ", operation='" + operation + '\'' +
                ", doorStatus=" + doorStatus +
                ", errorMessage='" + errorMessage + '\'' +
                "}\n";
    }
}
