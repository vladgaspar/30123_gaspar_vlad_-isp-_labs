package isp.lab2;

public class Exercise6 {

    /**
     * This method should generate the required vector non-recursively
     *
     * @param n the length of teh generated vector
     * @return the generated vector
     */
    public static int[] generateVector(int n) {
        int[] v = new int[1000];
        v[0] = 1;
        v[1] = 2;
        for (int i = 1; i <= n; i++){
            v[++i] = v[i] * v[--i];
        }
        return v;
    }

    /**
     * This method should generate the required vector recursively
     *
     * @param n the length of teh generated vector
     * @return the generated vector
     */
    public static int[] generateRandomVectorRecursively(int n) {
        throw new UnsupportedOperationException();
    }

    public static void main(String[] args) {
        // TODO: print the vectors
    }
}
