/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package isp.lab8.carparkaccess;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * @author mihai.hulea
 */
public class ParkingAccessControl {

    public static final int MAX_CAPACITY = 5;

    private ArrayList<Car> parkedCars = new ArrayList<Car>();

    private ArrayList<Car> previousCars = new ArrayList<Car>();

    private HashMap<String, Integer> priceingMap = new HashMap<String, Integer>();

    public void carEntry(Car car) {
        //1. sa verific capacitatea
        int cacpacity = parkedCars.size();
        System.out.println("The number of parked cars is:" + cacpacity);
        //2. daca capacitatea este depasita -> return
        if (cacpacity >= MAX_CAPACITY) {
            System.out.println("The parking area is full");
        } else {
            //3. verific daca nu cumva masina este deja in parcare, si daca este -> return
            if (cacpacity == 0) {
                System.out.println("Car " + car + " is entering car park!");
                parkedCars.add(car);
                car.entries++;
                priceingMap.put(car.getPlateNumber(), 0);
                car.setEntryTime(System.currentTimeMillis());
            } else {
                boolean ok = false;
                for (Car c : parkedCars) {
                    if (ok == false)
                        if (car.getPlateNumber().equals(c.getPlateNumber())) {
                            System.out.println("The car is already parked");
                            ok = true;
                            break;
                        }
                }
                //4. daca masina nu este in parcare salvez obiectul de tip car in parkedCars
                if (ok == false) {
                    System.out.println("Car " + car + " is entering car park!" + " \n");
                    parkedCars.add(car);
                    car.entries++;
                    priceingMap.put(car.getPlateNumber(), priceingMap.get(car.getPlateNumber()) + car.price);
                    car.setEntryTime(System.currentTimeMillis());
                }
            }
        }
    }

    public int carExit(String plateNumber) {
        //1. cautam dupa plate number un Car in parkedCars
        for (Car c : parkedCars) {
            if (plateNumber.equals(c.getPlateNumber())) {
                //2. daca am gasit masina,
                System.out.println("Car with plate number " + plateNumber + " is exiting");
                //calculez timpul de asteptare in parcare, -> System.currentTimeMiliseconds()
                //calculez pretul,
                int price = (int) (System.currentTimeMillis() - c.getEntryTime()) / 1000;
                c.price = c.price + price;
                //sterg masina din parkedCars si o adaug in previousCars
                parkedCars.remove(c);
                previousCars.add(c);
                priceingMap.put(plateNumber, priceingMap.get(plateNumber) + c.price);
                //returnez pretul
                return price;
            }
            //3. daca nu am gasit plateNumber -> return
        }
        System.out.println("Car not found");
        return 0;
    }

    public void viewCurrentCars() {
        System.out.println("\nDisplay all parked cars.");
        System.out.println("Avalabile spaces:" + (MAX_CAPACITY - parkedCars.size()));
        for (Car c : parkedCars) {
            System.out.println(c);
            System.out.println("Current cost:" + (System.currentTimeMillis() - c.getEntryTime()) / 1000 + "\n");
        }
    }

    public void viewPastEntriesForCar(String plateNumber) {
        System.out.println("\nDisplay all past entries for a car.");
        for (Car c : previousCars) {
            if (c.getPlateNumber().equals(plateNumber)) {
                System.out.println(c);
            }
        }
        System.out.println("Total priceing: " + priceingMap.get(plateNumber) + "\n");
    }

    public void viewPastUniqueEntries(String plateNumber) {
        System.out.println("\nDisplay all unique entries.");
        boolean ok = false;
        int entries = 0;
        for (Car c : previousCars) {
            if (c.getPlateNumber().equals(plateNumber)) {
                ok = true;
                entries= c.entries;
            }
        }
        if (ok == true)
            System.out.println(plateNumber + " " + entries);
    }

}
