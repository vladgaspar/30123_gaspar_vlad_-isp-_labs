package isp.lab8.carparkaccess;

import isp.lab8.carparkaccess.file.ParkingAccessFileControl;

import java.util.concurrent.TimeUnit;

public class CarParkingAccessApp {
    public static void main(String[] args) throws InterruptedException {
        System.out.println("It works!");

        ParkingAccessControl pac = new ParkingAccessControl();

        Car c1 = new Car("CJ-01-AAA", System.currentTimeMillis());
        Car c2 = new Car("CJ-05-EGS", System.currentTimeMillis());
        Car c3 = new Car("CJ-01-AJX", System.currentTimeMillis());
        Car c4 = new Car("CJ-01-WRS", System.currentTimeMillis());
        Car c5 = new Car("CJ-01-DMG", System.currentTimeMillis());
        Car c6 = new Car("CJ-01-TYC", System.currentTimeMillis());

       // pac.carEntry(c4);
        pac.carEntry(c2);
//        pac.carEntry(c3);
//        pac.carEntry(c4);
//        pac.carEntry(c5);
//        pac.carEntry(c6);

        pac.viewCurrentCars();

        TimeUnit.SECONDS.sleep(5);
        int price1 = pac.carExit("CJ-01-WRS");
        System.out.println("Price for parking is =" + price1 + "\n");

        TimeUnit.SECONDS.sleep(2);
        int price2 = pac.carExit("CJ-05-EGS");
        System.out.println("Price for parking is ="+price2 + "\n");

        c1.setEntryTime(System.currentTimeMillis());
        pac.carEntry(c4);

        TimeUnit.SECONDS.sleep(6);
        int pricez = pac.carExit("CJ-01-WRS");
        System.out.println("Price for parking is =" + pricez + "\n");


//
//        TimeUnit.SECONDS.sleep(2);
//        int pricex = pac.carExit("CJ-01-TYC");
//        System.out.println("Price for parking is ="+pricex);
//
//        TimeUnit.SECONDS.sleep(6);
//        int price3 = pac.carExit("CJ-01-AJX");
//        System.out.println("Price for parking is ="+price3);
//
//        TimeUnit.SECONDS.sleep(3);
//        int price4 = pac.carExit("CJ-01-WRS");
//        System.out.println("Price for parking is ="+price4);
//
//        TimeUnit.SECONDS.sleep(1);
//        int price5 = pac.carExit("CJ-01-DMG");
//        System.out.println("Price for parking is ="+price5);

        pac.viewPastEntriesForCar("CJ-05-EGS");

        ////////////////////////////////

//        ParkingAccessFileControl fpac = new ParkingAccessFileControl();
//        Car c2 = new Car("CJ-02-AAA", System.currentTimeMillis());
//        Car c3 = new Car("CJ-02-BBB", System.currentTimeMillis());
//        Car c4 = new Car("CJ-02-CCC", System.currentTimeMillis());
//        fpac.carEntry(c2);
//        fpac.carEntry(c3);
//        fpac.carEntry(c4);
//
//        price = fpac.carExit("CJ-02-CCC");
//
//        System.out.println("Price to be payed = "+price);
    }
}
