package isp.lab10.exercise1;

import java.util.List;

public class Aircraft extends Thread{

    private String id;
    private int altitude;
    private AtcCommand msg;
    private String state;


    public Aircraft(String id){
        this.id = id;
        state = "On stand";
    }

    public void reciveAtcCommand(AtcCommand msg){
        this.msg = msg;
    }

    @Override
    public void run() {

        synchronized (this){
            try {
                this.wait();
            } catch (InterruptedException e) {
            }
        }
        if(msg instanceof TakeoffCommand){
            state = "Taxing";
            try {
                Thread.sleep(10000);
            } catch (InterruptedException e) {
            }
        }
    }
}
//mecanism comunicare intre Threaduri