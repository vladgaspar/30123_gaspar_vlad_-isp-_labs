package isp.lab10.exercise1;

import java.util.ArrayList;
import java.util.List;

public class ATC {

    List<Aircraft> list = new ArrayList<Aircraft>();

    public void addAircraft(String id){
        Aircraft aircraft = new Aircraft(id);
        Thread thread = new Thread(aircraft);
        list.add(aircraft);
        aircraft.start();
    }


    public void sendCommand(String aicraftid, AtcCommand cmd){
        Aircraft aircraft = new Aircraft(aicraftid);
        aircraft.reciveAtcCommand(cmd);
        synchronized (aircraft){
            aircraft.notify();
        }
    }

    public void showAircrafts(){
        for (Aircraft aircraft: list){
            aircraft.toString();
        }
    }
}
