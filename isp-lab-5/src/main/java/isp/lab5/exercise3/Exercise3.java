package isp.lab5.exercise3;

interface Playable {
    void play();
}

class ColorVideo implements Playable {

    private String fileName;

    public ColorVideo(String fileName){
        this.fileName = fileName;
        loadFromDisk(fileName);
    }

    @Override
    public void play() {
        System.out.println("Play color video" + fileName);
    }

    private void loadFromDisk(String fileName){
        System.out.println("Loading video..." + fileName);
    }
}


class BlackAndWhiteVideo implements Playable {

    private String fileName;

    public BlackAndWhiteVideo(String fileName){
        this.fileName = fileName;
        loadFromDisk(fileName);
    }

    @Override
    public void play() {
        System.out.println("Play black and white video " + fileName);
    }

    private void loadFromDisk(String fileName){
        System.out.println("Loading video..." + fileName);
    }
}


class ProxyVideo implements Playable {

    private ColorVideo colorVideo;
    private BlackAndWhiteVideo blackAndWhiteVideo;
    private String fileName;
    public String type;

    public ProxyVideo(String fileName, String type){

        this.fileName = fileName;
        this.type = type;
    }

    @Override
    public void play() {
        if(colorVideo == null) {
            if (type.equals("Color") || type.equals("color")) {
                colorVideo = new ColorVideo(fileName);
                colorVideo.play();
            }
            else if(type.equals("Black and white") || type.equals("Black and White") || type.equals("black and white")) {
                blackAndWhiteVideo = new BlackAndWhiteVideo(fileName);
                blackAndWhiteVideo.play();
            }
            else{
                System.out.println("This type of video doesn't exist:" + this.type);
            }
        }
    }
}

public class Exercise3 {

    public static void main(String[] args) {

        ProxyVideo proxyVideo = new ProxyVideo("Batman", "color");
        proxyVideo.play();
    }
}



