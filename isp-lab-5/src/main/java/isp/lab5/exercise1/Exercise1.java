package isp.lab5.exercise1;


import java.util.ArrayList;

//////////////////////////////////////////
abstract class Transaction{
    protected Account account;

    public Transaction(Account account) {
        this.account = account;
    }

    abstract String execute();
}

//////////////////////////////////////////
class Withdraw extends Transaction{
    public double amount;

    public Withdraw(int amount, Account account) {
        super(account);
        this.amount = amount;

    }

    public double getAmount() {
        return amount;
    }

    public String execute(){
        if(account.getBalance()>amount){
            account.setBalance(account.getBalance()-amount);
            System.out.println("Transaction executed");
        }
        return "EXECUTED!";
    }
}

//////////////////////////////////////////
class Card{
    private String cardId;
    private String pin;

    public Card(String cardId, String pin) {
        this.cardId = cardId;
        this.pin = pin;
    }

    public String getCardId() {
        return cardId;
    }

    public void setCardId(String cardId) {
        this.cardId = cardId;
    }

    public String getPin() {
        return pin;
    }

    public void setPin(String pin) {
        this.pin = pin;
    }
}

//////////////////////////////////////////
class Account{
    private String owner;
    private double balance;
    private Card card;

    public Account(String owner, double balance, Card card) {
        this.owner = owner;
        this.balance = balance;
        this.card = card;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public Card getCard() {
        return card;
    }

    public void setCard(Card card) {
        this.card = card;
    }
}

//////////////////////////////////////////
class Bank{
    ArrayList<Account> list = new ArrayList<>();

    public void addAccount(Account a){
        list.add(a);
    }

    public void executeTransaction(Transaction t){
        t.execute();
    }

    public Account getAccountByCardId(String id){

        for(Account a:list){
            if(a.getCard().getCardId().equals(id))
                return a;
        }

        return null;
    }
}

//////////////////////////////////////////
class ATM{
    private Bank bank;
    private Card card;

    public ATM(Bank bank) {
        this.bank = bank;
    }

    public void insertCard(Card c, String pin){
        if(card==null){
            if(c.getPin().equals(pin)){
                System.out.println("Card accepted.");
                this.card = c;
            }else{
                System.out.println("Pin is not valid.");
            }
        }else{
            System.out.println("Card already inserted.");
        }
    }

    public void removeCard(){
        this.card = null;
    }

    public void withdraw(int amount){
        if(card!=null){
            Account acc = bank.getAccountByCardId(card.getCardId());
            Withdraw tw = new Withdraw(amount, acc);
            bank.executeTransaction(tw);

        }else{
            System.out.println("No card present.");
        }
    }

    public void changePin(String oldPin, String newPin){
        ChangePin changePin = new ChangePin(newPin,this.bank.getAccountByCardId(card.getCardId()));
        System.out.println(changePin.execute());
    }

    public void checkMoney(){
        CheckMoney checkMoney = new CheckMoney(this.bank.getAccountByCardId(card.getCardId()));
        System.out.println(checkMoney.execute());
    }

}


class ChangePin extends Transaction{

   public String newPin;
   public String oldPin;


   public ChangePin(String newPin, Account account){
       super(account);
       this.newPin = newPin;
   }

    @Override
    String execute() {
       if(this.account!=null){

           this.account.getCard().setPin(this.newPin);
       }
        return "Pin updated";
    }
}

class CheckMoney extends Transaction{

    public CheckMoney(Account account) {
        super(account);
    }

    @Override
    String execute() {
        if(this.account!=null){
            System.out.println("Your balance is: " + this.account.getBalance());
        }
        return "Done";
    }
}

//////////////////////////////////////////

public class Exercise1 {

    public static void main(String[] args) {
        Card c1 = new Card("12345","0000");
        Account a1 = new Account("Mihai",1000, c1);

        Bank bank = new Bank();
        bank.addAccount(a1);

        ATM atm = new ATM(bank);

        atm.insertCard(c1,"0000");

        atm.withdraw(900);

    }
}
