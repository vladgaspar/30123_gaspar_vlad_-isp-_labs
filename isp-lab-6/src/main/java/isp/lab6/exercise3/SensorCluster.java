package isp.lab6.exercise3;

import java.time.LocalDateTime;
import java.util.ArrayList;

public class SensorCluster {
    ArrayList<Sensor> sensors = new ArrayList<>();

    public void addSensor(String id, SensorType type){
        sensors.add(new Sensor(id, type));
    }

    public void writeSensorReading(String id, double value, long dateTime){
        for(Sensor s: sensors){
            if(s.id.equals(id)){
                s.addSensorReading(new SensorReading(value,dateTime));
            }
        }
    }

    public Sensor getSensorById(String id){
        return sensors.stream().filter(s -> s.id==id).findFirst().get();
    }

}
