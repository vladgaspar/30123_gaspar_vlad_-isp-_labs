package isp.lab6.exercise1;

import java.util.ArrayList;
import java.util.List;

public class Exercise1 {
    public static void main(String[] args) {


        SensorReadingList sensorReadingList = new SensorReadingList();
        sensorReadingList.addReading(new SensorReading(100,"Camera 215",Type.TEMPERATURE));
        sensorReadingList.addReading(new SensorReading(80,"Camera 112",Type.TEMPERATURE));
        sensorReadingList.addReading(new SensorReading(210,"Camera 201",Type.TEMPERATURE));
        sensorReadingList.addReading(new SensorReading(420,"Camera 215",Type.HUMIDITY));
        sensorReadingList.addReading(new SensorReading(500,"Camera 300",Type.PRESSURE));

        System.out.println("The average is: " + sensorReadingList.getAvarageValueByType(Type.TEMPERATURE,"Camera 215"));

        System.out.println(sensorReadingList.getReadingsByType(Type.TEMPERATURE));

        sensorReadingList.listSortedByLocation();

        sensorReadingList.listSortedByValue();

        System.out.println(sensorReadingList.findAllByLocationAndType("Camera 215", Type.HUMIDITY));

    }
}
