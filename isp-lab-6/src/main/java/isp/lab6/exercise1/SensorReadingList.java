package isp.lab6.exercise1;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class SensorReadingList implements IReadingRepository {

    public List<SensorReading> list = new ArrayList<SensorReading>();


    @Override
    public void addReading(SensorReading reading) {
        this.list.add(reading);
    }

    @Override
    public double getAvarageValueByType(Type type, String location) {

        double sum = 0;
        int count =0;

        for (SensorReading sensorReading : this.list)
            if (sensorReading.getType() == type && sensorReading.getLocation() == location) {
                sum += sensorReading.getValue();
                count++;
            }
        return sum / count;
    }

    @Override
    public List<SensorReading> getReadingsByType(Type type) {

        List<SensorReading> list = new ArrayList<>();

        for(SensorReading sensorReading : this.list)
            if(sensorReading.getType() == type)
                list.add(sensorReading);
        return list;
    }

    @Override
    public void listSortedByValue() {

        List<SensorReading> list = new ArrayList<>(this.list);

        list.sort(new Comparator<SensorReading>() {
            @Override
            public int compare(SensorReading o1, SensorReading o2) {
                return o1.getValue()-o2.getValue();
            }
        });

        System.out.println("\nSorted by Value: ");
        for(SensorReading sensorReading : list)
            System.out.println(sensorReading.toString());
    }

    @Override
    public void listSortedByLocation() {

        List<SensorReading> list = new ArrayList<>(this.list);

        list.sort(new Comparator<SensorReading>() {
            @Override
            public int compare(SensorReading o1, SensorReading o2) {
                return o1.getLocation().compareTo(o2.getLocation());
            }
        });

        System.out.println("\nSorted by Location: ");
        for(SensorReading sensorReading : list)
            System.out.println(sensorReading.toString());
    }


    @Override
    public List<SensorReading> findAllByLocationAndType(String location, Type type) {
        List<SensorReading> list = new ArrayList<>();

        for(SensorReading sensorReading : this.list)
            if(sensorReading.getType() == type && sensorReading.getLocation() == location)
                list.add(sensorReading);
        return list;
    }

    public void display() {
            System.out.println(list.toString());
    }
}


