package isp.lab6.exercise4;

public class ChangePin extends Transaction {
    public String newPin;
    public String oldPin;


    public ChangePin(String newPin, Account account){
        super(account);
        this.newPin = newPin;
    }

    @Override
    String execute() {
        if(this.account!=null){

            this.account.getCard().setPin(this.newPin);
        }
        return "Pin updated";
    }
}
