package isp.lab6.exercise4;

public class ATM {
    private Bank bank;
    private Card card;

    public ATM(Bank bank) {
        this.bank = bank;
    }

    public void insertCard(Card c, String pin){
        if(card==null){
            if(c.getPin().equals(pin)){
                System.out.println("Card accepted.");
                this.card = c;
            }else{
                System.out.println("Pin is not valid.");
            }
        }else{
            System.out.println("Card already inserted.");
        }
    }

    public void removeCard(){
        this.card = null;
    }

    public void withdraw(int amount){
        if(card!=null){
            Account acc = bank.getAccountByCardId(card.getCardId());
            Withdraw tw = new Withdraw(amount, acc);
            bank.executeTransaction(tw);

        }else{
            System.out.println("No card present.");
        }
    }

    public void changePin(String oldPin, String newPin){
        ChangePin changePin = new ChangePin(newPin,this.bank.getAccountByCardId(card.getCardId()));
        System.out.println(changePin.execute());
    }

    public void checkMoney(){
        CheckMoney checkMoney = new CheckMoney(this.bank.getAccountByCardId(card.getCardId()));
        System.out.println(checkMoney.execute());
    }

}
