package isp.lab6.exercise4;

public class Withdraw  extends Transaction{
    public double amount;

    public Withdraw(int amount, Account account) {
        super(account);
        this.amount = amount;
    }

    public double getAmount() {
        return amount;
    }

    public String execute(){
        if(account.getBalance()>amount){
            account.setBalance(account.getBalance()-amount);
            System.out.println("Transaction executed");
        }
        return "EXECUTED!";
    }
    }
