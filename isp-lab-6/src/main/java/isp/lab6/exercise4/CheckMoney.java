package isp.lab6.exercise4;

public class CheckMoney extends Transaction {
    public CheckMoney(Account account) {
        super(account);
    }

    @Override
    String execute() {
        if(this.account!=null){
            System.out.println("Your balance is: " + this.account.getBalance());
        }
        return "Done";
    }
}
