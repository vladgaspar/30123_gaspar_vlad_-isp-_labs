package isp.lab6.exercise4;

import java.util.ArrayList;

public class Bank {
    ArrayList<Account> list = new ArrayList<>();

    public void addAccount(Account a){
        list.add(a);
    }

    public void executeTransaction(Transaction t){
        t.execute();
    }

    public Account getAccountByCardId(String id){

        for(Account a:list){
            if(a.getCard().getCardId().equals(id))
                return a;
        }

        return null;
    }
}
