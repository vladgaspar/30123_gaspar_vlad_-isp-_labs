package isp.lab6.exercise2;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class EquipmentController {

    List<Equipment> list = new ArrayList<>();

    /**
     * Add new equipment to the list of equipments
     *
     * @param equipment - equipment to be added
     */
    public void addEquipment(final Equipment equipment) {
        this.list.add(equipment);
    }

    /**
     * Get current list of equipments
     *
     * @return list of equipments
     */
    public List<Equipment> getEquipments() {
        return this.list;
    }

    /**
     * Get number of equipments
     *
     * @return number of equipments
     */
    public int getNumberOfEquipments() {

        int count = 0;

        for (Equipment equipment : list) {
            if (equipment.getSerialNumber() != null)
                count++;
        }

        return count;
    }

    /**
     * Group equipments by owner
     *
     * @return a dictionary where the key is the owner and value is represented by list of equipments he owns
     */
    public Map<String, List<Equipment>> getEquipmentsGroupedByOwner() {
        Map<String, List<Equipment>> map = new HashMap<>();

        for (Equipment equipment : this.list) {
            if (!map.containsKey(equipment.getOwner()))
                map.put(equipment.getOwner(), new ArrayList<Equipment>());
            map.get(equipment.getOwner()).add(equipment);
        }
        return map;
    }

    /**
     * Remove a particular equipment from equipments list by serial number
     *
     * @param serialNumber - unique serial number
     * @return deleted equipment instance or null if not found
     */
    public Equipment removeEquipmentBySerialNumber(final String serialNumber) {

        for (Equipment equipment : this.list)
            if (equipment.getSerialNumber() == serialNumber) {
                this.list.remove(equipment);
                return equipment;
            }
        return null;
    }
}

