package isp.lab6.exercise2;

public class Exercise2 {
    public static void main(String[] args) {

        Equipment equipment1 = new Equipment("Dell","123");

        System.out.println(equipment1.isTaken());
        System.out.println(equipment1.getOwner());

        Equipment equipment2 = new Equipment("Dell","123","alex");

        System.out.println(equipment2.isTaken());
        System.out.println(equipment2.getOwner());



    }
}
